package tarot;

import java.util.List;

import cardset.Card;
import cardset.Hand;
import cardset.SortCardMethod;

class TarotField extends Hand {

	public TarotField(String name, SortCardMethod method) {
		super(name, method);
	}

	@Override
	public void print() {
		List<Card> filedCard = getHandCards();

		filedCard.stream().forEach(c ->
		System.out.println(String.format("%s：%d(%s)",
				c.getKind(), c.getNum(), c.getValue() == 0 ? "裏": "表")));
	}

	@Override
	public int getPoint() {
		// TODO 自動生成されたメソッド・スタブ
		return 0;
	}

	@Override
	public void printCardPoint() {
		print();

		List<Card> filedCard = getHandCards();
		long frontCard = filedCard.stream().filter(c -> c.getValue() == 1).count();

		System.out.println("------表が"+frontCard+"枚-------");
		System.out.println("答えは"+
		((frontCard > filedCard.size()/2) ? "Yes": "No" )+ "です");
	}

}
