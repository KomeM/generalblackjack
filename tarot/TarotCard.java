package tarot;

import java.util.Random;

import cardset.Card;

class TarotCard extends Card {

	private int value;
	private int num;

	public TarotCard(int id, long seed) {
		super(id);
		Random random = new Random(seed);

		if (id < 22) {
			setKind("大アルカナ");
			num = id;
			value = random.nextInt(2); // 1か0
			return;
		}

		int shortAlcana = id - 22;

		switch(shortAlcana / 14) {
		case 0:
			setKind("ワンド");
			break;
		case 1:
			setKind("ソード");
			break;
		case 2:
			setKind("カップ");
			break;
		case 3:
			setKind("コイン");
			break;
		default:
			setKind("jorker");
			break;
		}
		num = shortAlcana % 14 + 1;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public int getNum() {
		return num;
	}
}
