package tarot;

import cardset.Deck;
import cardset.Factory;
import cardset.Hand;
import cardset.NoSortCardMethod;

public class Main {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		int DeckSize = 78;
		Factory factory = new TarotCardFactory();
		Deck deck = factory.createDeck(DeckSize);

		deck.shuffle();
		Hand filed = new TarotField("filed", new NoSortCardMethod());

		filed.addCard(deck.draw(), true);
		filed.addCard(deck.draw(), true);
		filed.addCard(deck.draw(), true);
		filed.addCard(deck.draw(), true);
		filed.addCard(deck.draw(), true);

		filed.printCardPoint();
	}

}
