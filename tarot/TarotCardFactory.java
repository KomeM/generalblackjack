package tarot;

import java.util.stream.IntStream;

import cardset.Deck;
import cardset.DequeDeck;
import cardset.Factory;

class TarotCardFactory implements Factory {
	public TarotCardFactory() {}

	public Deck createDeck(int deckSize) {
		Deck deck = new DequeDeck();
		long seed = 100;

		IntStream.range(0, deckSize)
			.forEach(id -> deck.addCard(new TarotCard(id, seed)));

		return deck;
	}
}
