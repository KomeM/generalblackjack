package blackjack;

public class Main {

	public static void main(String[] args) {
		int cardNum = 52;

//		Factory factory = new BlackJackFactory();
//		Deck deck = factory.createDeck(cardNum);
//
//		deck.shuffle();
//		Hand  player = new PlayerHand("user");
//
//		player.addCard(deck.draw(), true);
//		player.addCard(deck.draw(), true);
//		player.addCard(deck.draw(), true);
//		player.addCard(deck.draw(), true);
//
//		player.print();
//
//		player.sort();
//
//		player.print();

		Board game = new Board(new PlayerHand("dealer"));
		game.addPlayer(new PlayerHand("user"));

		game.init(cardNum);

		game.start();

	}

}
