package blackjack;

import java.util.stream.IntStream;

import cardset.Deck;
import cardset.Factory;
import cardset.DequeDeck;

class BlackJackFactory implements Factory{
	public BlackJackFactory() {}

	public Deck createDeck(int deckSize) {
		Deck deck = new DequeDeck();

		IntStream.range(0, deckSize)
			.forEach(j -> deck.addCard(new BlackJackCard(j%13 + 1, j%4)));

		return deck;
	}
}
