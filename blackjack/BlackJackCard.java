package blackjack;

import cardset.TrumpCard;

class BlackJackCard extends TrumpCard {

	public BlackJackCard(int value, int kind) {
		super(value, kind);
	}

	public int getValue() {
		if (getNum() > 10) {
			return 10;
		}
		else {
			return getNum();
		}
	}
}
