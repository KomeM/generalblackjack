package blackjack;

import java.util.Arrays;

import cardset.Hand;
import cardset.Judge;

class BlackJackJudge implements Judge {
	public Hand[] calcWinner(Hand...hand) {

		Hand winner = Arrays.asList(hand).stream()
							.filter(h -> h.getPoint() <= 21)
							.max((l, r) -> l.getPoint() - r.getPoint()).get();


		return Arrays.asList(hand).stream()
				.filter(h -> h.getPoint() == winner.getPoint())
				.toArray(PlayerHand[]::new);
	}
}
