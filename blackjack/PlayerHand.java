package blackjack;

import java.util.List;
import java.util.Map;

import cardset.Card;
import cardset.Hand;
import cardset.NoSortCardMethod;

class PlayerHand extends Hand {

	public PlayerHand(String name) {
		super(name, new NoSortCardMethod());
	}

	public void printCardPoint() {
		print();
		System.out.println("----得点(" + getPoint() + ")-----\n");
	}

	public void print() {
		System.out.println(getName() + "の手札は");

		List<Card> handCards = getHandCards();
		Map<Integer, Boolean> openCards = getOpenCards();

		handCards.stream()
			.filter(card -> openCards.get(card.getId()))
			.forEach(card ->
				System.out.println(card.getKind() + " " + card.getNum()));
	}

	public int getPoint() {
		int score = 0;
		boolean oneCheck = false;
		for (Card card: getHandCards()) {
			if (card.getNum() == 1) {
				oneCheck = true;
			}
			score += card.getValue();
		}

		if (oneCheck && score + 10 <= 21) {
			score += 10;
		}
		return score;
	}
}
