package blackjack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import cardset.Deck;
import cardset.Factory;
import cardset.Hand;
import cardset.Judge;

class Board {

	List<Hand> players = new ArrayList<>();
	Deck deck;
	Factory factory;
	Judge judge;

	public Board(Hand dealer) {
		players.add(dealer);

		factory = new BlackJackFactory();
		judge = new BlackJackJudge();

	}

	public void addPlayer(Hand player) {
		this.players.add(player);
	}

	public void init(int cardNum) {
		deck = factory.createDeck(cardNum);
		deck.shuffle();

		players.get(0).addCard(deck.draw(), true);
		players.get(0).addCard(deck.draw(), false);

		for(int index = 1; index < players.size(); ++ index) {
			players.get(index).addCard(deck.draw(), true);
			players.get(index).addCard(deck.draw(), true);
		}
	}

	public boolean isDraw() {
		String select = "";
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);

		while (!select.equals("y") && !select.equals("n")) {
			System.out.println("引きますか？(y/n)");
			select = scan.next();
		}

		return select.equals("y") ? true: false;
	}

	public void playerTurn(Hand player) {
		player.printCardPoint();

		while(player.getPoint() < 21 && isDraw()) {
			player.addCard(deck.draw(), true);

			player.printCardPoint();
		}
	}

	public void cpuTurn(Hand player) {
		while(player.getPoint() < 17) {
			player.addCard(deck.draw(), true);
		}
		player.openAllCards();
	}

	public void result() {
		System.out.println("\n----結果発表!!!----\n");

		for (Hand player: players) {
			player.printCardPoint();
		}

		Hand[] winner = judge.calcWinner(players.toArray(new Hand[players.size()]));

		if (winner.length == 2 || winner.length == 0) {
			System.out.println("引き分け");
		}
		else {
			Arrays.asList(winner)
				.forEach(hand -> System.out.println("勝者は: "+ hand.getName()));

		}
	}

	public void start() {
		players.get(0).print();
		System.out.println();
		playerTurn(players.get(1));
		cpuTurn(players.get(0));
		result();
	}

}
