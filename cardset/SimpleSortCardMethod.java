package cardset;

public class SimpleSortCardMethod implements SortCardMethod {
	public int compare (Card c1, Card c2) {
		return c1.getNum() - c2.getNum();
	};
}
