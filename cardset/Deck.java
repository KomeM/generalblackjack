package cardset;

public interface Deck {
	void shuffle();
	void addCard(Card c);
	int getSize();
	Card draw();
}

