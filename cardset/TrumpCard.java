package cardset;

public abstract class TrumpCard extends Card {

	private int num;

	public int getNum() {
		return num;
	}

	abstract public int getValue();

	public TrumpCard(int num, int kind) {
		super(num*(kind + 1));

		this.num = num;

		switch(kind) {
		case 0:
			setKind("スペード");
			break;
		case 1:
			setKind("クローバ");
			break;
		case 2:
			setKind("ハート");
			break;
		case 3:
			setKind("エース");
			break;
		default:
			setKind("ジョーカー");
			break;
		}
	}
}
