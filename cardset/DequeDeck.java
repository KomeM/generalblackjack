package cardset;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DequeDeck implements Deck {
	private List<Card> cards = new LinkedList<>();

	public DequeDeck() {}

	public void addCard(Card c) {
		cards.add(c);
	}

	public void shuffle () {
		Collections.shuffle(cards);
	}

	public int getSize() {
		return cards.size();
	}

	public Card draw() {
		return ((LinkedList<Card>) this.cards).pop();
	}
}
