package cardset;

import java.util.Comparator;

public interface SortCardMethod extends Comparator<Card> {
	int compare (Card c1, Card c2);
}
