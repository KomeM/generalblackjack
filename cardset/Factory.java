package cardset;

public interface Factory {
	Deck createDeck(int DeckSize);
}
