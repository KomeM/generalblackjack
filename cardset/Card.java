package cardset;

public abstract class Card {

	abstract public int getValue();
	abstract public int getNum();

	private String kind;
	private int id;

	protected Card(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	protected void setKind(String kind) {
		this.kind = kind;
	}

	public String getKind() {
		return kind;
	}
}
