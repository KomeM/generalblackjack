package cardset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hand {

	private List<Card> handCards = new ArrayList<>();
	private Map<Integer, Boolean> openCards = new HashMap<>();
	private String name;
	private SortCardMethod sortMethod;

	public abstract void print();
	public abstract int getPoint();
	public abstract void printCardPoint();

	protected Hand(String name, SortCardMethod method) {
		this.name = name;
		this.sortMethod = method;
	}

	public void sort() {
		handCards.sort(sortMethod);
	}

	public void shuffle() {
		Collections.shuffle(handCards);
	}

	public int getSize() {
		return handCards.size();
	}


	protected  List<Card> getHandCards() {
		return handCards;
	}

	protected Map<Integer, Boolean> getOpenCards() {
		return openCards;
	}

	public String getName() {
		return name;
	}

	public int getCardNum() {
		return handCards.size();
	}


	public void addCard(Card card, boolean open) {
		handCards.add(card);
		openCards.put(card.getId(), open);
	}

	public void openCard(int index) {
		openCards.replace(handCards.get(index).getId(), true);
	}

	public void openAllCards() {
		openCards.replaceAll((i, b) -> true);
	}

	public void removeCard(int index) {
		openCards.remove(handCards.get(index).getId());
		handCards.remove(index);
	}
}
